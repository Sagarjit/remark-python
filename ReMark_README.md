# Face Recognization & Mark Attendance System (ReMark) 
Facial Recognization & Mark Attendance System captures the 3D facial image and marks attendance by analyzing the 
similarity with the previously registered faces.   

## Why ReMark? 

The issue of attendance management of students/teachers in any institution is a very lengthy process and even time consuming.
Furthermore, biometrics attendance system is also available. These methods too are time consuming, since each time
student/teacher has to form a queue for scanning their thumb using real time camera. Now according to our proposed system, 
the attendance will be recorded by using camera, which will capture images of students/staff. It will then compare the
faces with the Studentís /Staff database and marks the attendance. Such automated systems will help largely reduce the 
manual labor and discrepancies otherwise involved in attendance maintenance. 

## Techniques and Technologies

Face detection and face recognition are very advanced in terms of computer authentication technology.
Face detection helps in determining the location and sizes of human faces in an image. It detects faces and ignores anything 
else, such as building, chairs, and trees. It is a starting point for face recognition. Most of the face detection methods 
focus on detecting frontal faces.The Face Recognition process is done by comparing the extracted features from the image with 
the one previously stored in the database.The system is going to work by some techniques such as the image is captured by  
camera of android application and is then processed towards the detection, as the detected face image is obtained , face 
recognition has to be done which is divided into further parts namelyface alignment, preprocessing, feature extraction, 
face matching where the image is converted into gray scale image and the result has to be seen.



## Modules 
__**Module1:**__ 
   
Front End(Android) - user Interaction with application
The authorized users will be able to mark their attendance by capturing their face image on the proposed system/application which 
will result in successful atendance mark, if the  captured image matches with one stored in the database library. 

__**Module2:**__

Back End(Python) - Image processing and analysis
The captured image will be detected, analyzed and then recognized based on some statistical methods of machine 
learning/artificial inteligence which will then help in identifying the correct person whose attendance has to be marked.

__**Module3:**__
   
Data End(Oracle) - user data storage
The authorized users credentials and other personal details will be stored in a secure centralized data store.


## Tools and Softwares
## Tool name 				      Version 
   Android Studio
   PyCharm, Python			      PyCharm 2016.3.6, Python3.x
   Oracle						  11g (11.2.0.2.0) Express Edition


   
## Softwares Description and installation :
 

#### JAVA

    1. Go to the Java Software Development Kit website.
    2. Click DOWNLOAD under the "JDK" heading.(Select JDK8)
    3. Check the "Accept License Agreement" box, then select your operating system's link.
    4. Double-click the downloaded installation file.
    5. Follow the on-screen installation instructions. On Windows and Linux, you must set the JDK path as well.
    For more ref. follow: https://www.wikihow.com/Install-the-Java-Software-Development-Kit


#### Android
    1. Download Android Studio 2.3.3 from the link: https://developer.android.com/studio/#downloads
    2. Then install it.
    3. Download SDK tools and install from the link: https://developer.android.com/studio/#downloads
    4. Update the apprropriate API version according to the implementation.

 
#### PYTHON
    Before going to the installation process, please read the OPENCV section
    1. Download Python 3.x (3.6 or 3.7).
       you can also refer the link : [GitHub](http://hep-outreach.uchicago.edu/samples/python_setup/) 
    2. Set the environment variables: set the PATH variable (in system variables) by defining the path of python where it 
       is installed on the system.
   

#### PyCharm
    1. Download PyCharm 2016.3.6 from the below link -
       [GitHub](https://www.jetbrains.com/pycharm/download/previous.html)
    2. Install it.

#### Oracle

    1. Download Oracle Database Express Edition 11g -32bits if you are installing Python 32bit version
    Or Download Oracle Database Express Edition 11g -64bits if you are installing Python 64bit version
    link: ''' http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html '''
    2. Install the package and then set environment variable path in system variable:
       C:\oraclexe\app\oracle\product\11.2.0\server\bin


      
## Libraries needs to be installed for Python:

__**OPENCV**__

If you are using win7(32 bits) then any version of python(2.7.x-32bits or 3.x-32bits) will be supported. After 
installing the python, you need to install OPENCV. There are two ways to install opencv:

    1. You can install through pip command: C:\Python36-32\Scripts> pip install opencv-python
    2. By downloading the opencv.exe and setting the path of opencv in environment variable which indicates the 
      path upto cv.pyd. cv.pyd will be copied from opencv to site-packages where python is installed.

If you are using win7(64bits) then you have to install python2.7.15(32 bits) and opencv version will be 3.1.0. 
No other will not fit for this case. Otherwise you will get "DLL load failed" error. 
There are two ways to install opencv:
   
    1. You can install through pip command: C:\Python36-32\Scripts> pip install opencv-python
    2. By downloading the opencv.exe and setting the path of opencv in environment variable(PATH variable) which 
      indicates the path upto cv.pyd and this .pyd will be copied from opencv to site-packages where python is installed.
      path: C:\opencv\build\python\2.7\x86;

In order to support the cv2.createLBPHFaceRecognizer() we need to install opencv version 2.4.13.6
      
__**Cx_Oracle**__

cx_Oracle is required to install in ython to make connection between Oracle and python.
To install this package type command: :\Python36-32\Scripts> pip install cx_Oracle

      








## Libraries needs to be installed for Android:

