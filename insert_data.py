import cx_Oracle
from pathlib import Path
import datetime

con = cx_Oracle.connect('system/tiger@127.0.0.1/xe')

def db_insert(my_file1,my_file2,my_file3,my_file4,my_file5):
    cur = con.cursor()
    print("Cursor-",cur)
    print("Image path-", my_file1)
    img_file1 = my_file1.open("rb").read()
    img_file2 = my_file2.open("rb").read()
    img_file3 = my_file3.open("rb").read()
    img_file4 = my_file4.open("rb").read()
    img_file5 = my_file5.open("rb").read()
    print("img_file--",img_file1)
    ts = datetime.datetime.now().strftime('%y-%m-%d  %H:%M:%S')
    create_ts = datetime.datetime.strptime(ts, '%y-%m-%d  %H:%M:%S')
    row = ('1513605', 'ramana.kotlin@tcs.com', 'Ramana Kotlin', '02/06/1993', 'California University', 'Computer Science and Engineering', 'Assistant Professor',  img_file1, img_file2, img_file3, img_file4, img_file5, create_ts)
    statement = 'INSERT INTO REMARK_EMP_REG_INFO(EMP_ID, EMP_MAIL, EMP_NAME, EMP_DOB, EMP_CLGNAME, EMP_DEPT, EMP_DEGN, EMP_FACE_IMG1, EMP_FACE_IMG2, EMP_FACE_IMG3, EMP_FACE_IMG4, EMP_FACE_IMG5, CREATE_TS) values(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13)'
    cur.execute(statement, row)
    print("After execute")
    con.commit()
    cur.close()
    con.close()


def main():
    try:
        # path = 'C:\\Users\HP\Desktop\photo.jpg'
        my_file1 = Path('C:\\Users\\HP\\Desktop\\test_images\\myface1.jpg')
        my_file2 = Path('C:\\Users\HP\\Desktop\\test_images\\myface2.jpg')
        my_file3 = Path('C:\\Users\HP\\Desktop\\test_images\\myface3.jpg')
        my_file4 = Path('C:\\Users\\HP\\Desktop\\test_images\\myface4.jpg')
        my_file5 = Path('C:\\Users\\HP\\Desktop\\test_images\\myface5.jpg')
        print("Image path-",my_file1)
        # filename = os.path.basename(path)
        # jpgimg = Image.open("C:\\Users\HP\Desktop\photo.jpg")
        # with open("C:\\Users\HP\Desktop\me.png", "rb") as imageFile:
           # str = base64.b64encode(imageFile.read())
        db_insert(my_file1,my_file2,my_file3,my_file4,my_file5)
    except IOError:
        pass

if __name__ == "__main__":
    main()