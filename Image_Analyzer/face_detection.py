#opencv- 3.1.0 and Python2.7.15

import sys
import cx_Oracle
import os
import datetime
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from PIL import Image


conn = cx_Oracle.connect('system/tiger@127.0.0.1/xe')
cur = conn.cursor()
#EmpId and Images search in Oracle and store in temp directory of server path
def db_search_oracle_img_to_tempDir():
    strt_time = datetime.datetime.now()
    table_name = 'MSTR_EMP_REG_INFO'
    eid = 'emp_id'
    face_image = 'EMP_FACE_IMG'
    stmt = 'SELECT {1},{2} FROM {0}'.format(table_name,eid,face_image)
    cur.execute(stmt)
    row = cur.fetchall()
    dict_id_blob = dict(row)
    print("Dict_id_blob--> ",dict_id_blob)
    for id,blob in dict_id_blob.items():
        path = "C:\\Users\HP\\Desktop\\oracle_temp_db\\" + id+ ".bmp"
        imageFile = open(path, 'wb')
        imageFile.write(blob.read())
        imageFile.close()
    #imageBlob = row[0]
    cur.close()
    conn.close()
    end_time = datetime.datetime.now()
    print("Time diff-->",end_time - strt_time)

    #oracle_img_to_tempDir(row)
'''     for column_1 in cur.fetchall():
            #print (column_1, "\t", column_2, "\t", column_3, "\t", column_4)
             print(column_1)
        cur.close()
        conn.close()
'''
'''
def oracle_img_to_tempDir(row):
    print("Row Row:  ",row)
    dict_id_blob = dict(row)
    print("Dictionary ID & Blob: ",dict_id_blob)
    print("row count--", cur.rowcount)
    for row1 in cur.fetchall():
        print("Updated row--",row1)
    #empID_from_tuple_list = [i[0] for i in row]
    #blob_from_tuple_list = [i[1] for i in row]
    #print("Only Blob",blob_from_tuple_list)
    #print("empID_from_tuple_list",empID_from_tuple_list)
    #print("blob_from_tuple_list",blob_from_tuple_list)
    #imageBlob = row[0]
    for id,blob in dict_id_blob.items():
        path = "C:\\Users\HP\\Desktop\\oracle_temp_db\\" + id+ ".bmp"
        imageFile = open(path, 'wb')
        imageFile.write(blob.read())
        imageFile.close()
'''
#Fetching employee id from imagename & path of image from oracle_tempDir
def empID_path_From_oracle_tempDir():
    print("\n")
    ora_img_user1 = "C:\\Users\HP\Desktop\oracle_temp_db\\1513605.bmp"
    ora_img_user2 = "C:\\Users\HP\Desktop\oracle_temp_db\\1513606.bmp"
    ora_img_user3 = "C:\\Users\HP\Desktop\oracle_temp_db\\1513607.bmp"
    ora_img_user4 = "C:\\Users\HP\Desktop\oracle_temp_db\\1513611.bmp"
    list_images = [ora_img_user1, ora_img_user2, ora_img_user3, ora_img_user4]
    print("List images of oracle images-->", list_images)
    empid_imgPath = {}
    for each_img in list_images:
        filename = os.path.basename(each_img)
        print("Filename from tempDB-->", filename)
        id, extn = filename.split('.')
        empid_imgPath[id] = each_img
        print("Employee ID from tempDB-->", id)
        # print("Photo extension-->", extn)
    print("All EmpID with path from tempDir-->", empid_imgPath)
    return empid_imgPath




#Fetching employee id from imagename & path of image from localDir
def empID_path_From_localDir():
    print("\n")
    imgs_captured_by_user1 = "C:\\Users\HP\Desktop\\app_local_db\\1513605.bmp"
    imgs_captured_by_user2 = "C:\\Users\HP\Desktop\\app_local_db\\1513606.bmp"
    imgs_captured_by_user3 = "C:\\Users\HP\Desktop\\app_local_db\\1513607.bmp"
    imgs_captured_by_user4 = "C:\\Users\HP\Desktop\\app_local_db\\1513611.bmp"
    list_images = [imgs_captured_by_user1, imgs_captured_by_user2, imgs_captured_by_user3, imgs_captured_by_user4]
    print("List images of App-->",list_images)
    empid_imgPath = {}
    for each_img in list_images:
        filename = os.path.basename(each_img)
        print("Filename from localDir-->", filename)
        id, extn = filename.split('.')
        empid_imgPath[id] = each_img
        print("Employee ID from localDir-->",id)
    print("All EmpID with path from local_dir-->", empid_imgPath)
    return empid_imgPath
    #print("Only employee Id from images in dictionary-->",empid.keys())
    #print("Row from Oracle-->",row)
    #id_blob_list = [item for item in row if item[0] == empid]
    #print(id_blob_list)
    # [x[1] for x in list]


def face_compare():
    dict_from_tempDir = empID_path_From_oracle_tempDir()
    dict_from_localDir = empID_path_From_localDir()
    print("From Compare-->",dict_from_tempDir)
    print(dict_from_localDir)
    #imageA = dict_from_tempDir['1513611']
    #imageB = dict_from_localDir['1513611']

    imageA = cv2.imread('C:\\Users\HP\Desktop\\app_local_db\\1513611.bmp')
    imageB = cv2.imread('C:\\Users\\HP\\Desktop\\oracle_temp_db\\1513611.bmp')
    #imageA = Image.open('C:\\Users\\HP\\Desktop\\app_local_db\\1513611.bmp')
    #imageB = Image.open('C:\\Users\\HP\\Desktop\\oracle_temp_db\\1513611.bmp')

    #imageA = mpimg.imread('C:\\Users\\HP\\Desktop\\151.jpg')
    #imageB = mpimg.imread('C:\\Users\\HP\\Desktop\\123.jpg')

    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])
    print("MSE-->",err)







def main():
    try:
        db_search_oracle_img_to_tempDir()
        face_compare()
    except IOError:
        pass

if __name__ == "__main__":
    main()