import cx_Oracle
import sys
sys.path.append("Image_Analyzer/")
import os
import cv2
import numpy as np
import shutil

import config

conn = cx_Oracle.connect('system/tiger@127.0.0.1/xe')
cur = conn.cursor()

def db_search_img_to_dict():
    #oracle images from database to python dictionary
    stmt = 'SELECT {1},{2},{3},{4},{5},{6} FROM {0}'.format(config.reg_table,config.emp_id,config.face_image1,config.face_image2, config.face_image3,config.face_image4,config.face_image5)
    cur.execute(stmt)
    row = cur.fetchall()
    list_id_clob = row
    print("Lict_id_clob--> ",list_id_clob)
    empID_from_tuple_list = [i[0] for i in row]
    print empID_from_tuple_list
    clob_from_tuple_list = [i[1:6] for i in row]
    print clob_from_tuple_list
    dict_id_clob = dict((z[0],list(z[1:])) for z in zip(empID_from_tuple_list, clob_from_tuple_list))
    print("db_search_oracle_img_to_dict",  dict_id_clob)
    oracle_img_dict_to_tempDir(dict_id_clob)


def oracle_img_dict_to_tempDir(dict_id_clob):
    print ('oracle_img_dict_to_tempDir-->',dict_id_clob)
    for eid in dict_id_clob.keys():
        sub_folder = "C:\\Users\HP\\Desktop\\oracle_temp_dir\\" + "E" + eid
        if os.path.exists(sub_folder):
            shutil.rmtree(sub_folder)
        for clobs in dict_id_clob[eid]:
            print('CLOBS-->',clobs)
            os.mkdir(sub_folder)
            #os.mkdir("training_data\\" + eid)
            for i in range(5):
                #path = "C:\\Users\HP\\Desktop\\oracle_temp_db\\" + eid + "\\"+ eid + "_" + str(i + 1) +".bmp"
                path = sub_folder + "\\" + eid + str(i + 1) + ".jpg"
                #path = "training_data\\" + eid + "\\" + str(i) + ".jpg"
                imageFile = open(path, 'wb')
                imageFile.write(clobs[i].read())
                imageFile.close()


#function to detect face using OpenCV
def detect_face(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    print gray
    face_cascade = cv2.CascadeClassifier('lbpcascade_frontalface.xml')
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=5)
    if (len(faces) == 0):
        return None, None
    (x, y, w , h) = faces[0]
    return gray[y:y+w, x:x+h], faces[0]



def prepare_training_data(temp_folder_path):
    print("data_folder_path-->", temp_folder_path)
    dirs = os.listdir(temp_folder_path)
    print dirs
    faces = []
    labels = []
    for dir_name in dirs:
        if not dir_name.startswith("E"):
            continue
        label = int(dir_name.replace("E", ""))
        emp_dir_path = temp_folder_path + "\\" + dir_name
        print("emp_dir_path-->",emp_dir_path)
        emp_images_names = os.listdir(emp_dir_path)
        print("emp_images_names-->",emp_images_names)
        for image_name in emp_images_names:
            # ignore system files like .DS_Store
            if image_name.startswith("."):
                continue
            image_path = emp_dir_path + "\\" + image_name
            print("Image path-->",image_path)
            image = cv2.imread(image_path)
            cv2.imshow("Training on image...", image)
            cv2.waitKey(300)
            # detect face
            face, rect = detect_face(image)
            #cv2.imshow('img', face)
            faces.append(face)
            labels.append(label)
    print("Faces-",faces)
    print ("Labels-",labels)
    return faces, labels


def prepare_test_data():
    test_data_path = 'C:\\Users\HP\\Desktop\\local_face_dir'
    faces = []
    profile_ids = []
    unknown_images_names = os.listdir(test_data_path)
    print("Unknown images--",unknown_images_names)
    for image_name in unknown_images_names:
        # ignore system files like .DS_Store
        if image_name.startswith("."):
            continue
        image_path = test_data_path + "\\" + image_name
        path, filename = os.path.split(image_path)
        profile_ids.append(filename)
        image = cv2.imread(image_path)
        cv2.imshow("Testing on image...", image)
        cv2.waitKey(500)
        face, rect = detect_face(image)
        faces.append(face)
    print("Profile ID-->",profile_ids)
    return faces, profile_ids


def predict_img(unknown_faces_list, profile_ids, face_recognizer_model):
    labels = []
    prof_ids = profile_ids
    final_result = {}
    print("unknown faces from predict method--",unknown_faces_list)
    for face in unknown_faces_list:
        print("face>>",face)
        label = face_recognizer_model.predict(face)
        #print("After prediction, Label-->",label)
        labels.append(label)
    final_result = dict((z[0], list(z[1:])) for z in zip(prof_ids, labels))
    return final_result



'''def predict(test_img, face_recognizer):
    img = test_img.copy()
    face, rect = detect_face(img)
    label = face_recognizer.predict(face)
    return label'''

def main():
    try:
        db_search_img_to_dict()
        #preparation of training data
        training_data_path = 'C:\\Users\HP\\Desktop\\oracle_temp_dir'
        print("Preparing training dataset...")
        faces, labels = prepare_training_data(training_data_path)
        print("Traing Data prepared")
        # print total faces and labels
        print("Total faces: ", len(faces))
        print("Total labels: ", len(labels))

        # create our LBPH face recognizer
        face_recognizer = cv2.createLBPHFaceRecognizer()
        face_recognizer.train(faces, np.array(labels))
        print face_recognizer

        #preparation of testing data
        print("Preparing test data...")
        faces_test_images, profile_ids = prepare_test_data()
        print("IDs--",profile_ids)
        predicted_result_dict = predict_img(faces_test_images, profile_ids, face_recognizer)
        print("Predicted Level-->",predicted_result_dict)

        '''test_img_path = 'C:\\Users\HP\\Desktop\\local_face_dir\\face1.jpg'
        test_img = cv2.imread(test_img_path)
        predicted_level = predict(test_img, face_recognizer)
        print("****Predicted Level******->",predicted_level)'''

    except IOError:
        pass

if __name__ == "__main__":
    main()
