import datetime
import cx_Oracle
import tempfile
import smtplib
import email.message
from email.encoders import encode_base64
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart

#today = datetime.datetime.now().strftime('%d-%m-%y %H:%M:%S')
now = datetime.datetime.now()

rows=[('1513605','Sagarjit \tDas','07/10/2018 \t9:45 AM','Marked'),
      ('1513610','Ramana \tKotlin','07/10/2018 \t9:39 AM','Marked'),
      ('1513612', 'Deepika \tBansal', '07/10/2018 \t9:32 AM', 'Marked'),
      ('1513613', 'Manik \tGupta', '07/10/2018 \t9:47 AM', 'Marked'),
      ('1513614', 'Nand \tKishor', '07/10/2018 \t9:55 AM', 'Marked')]

html = """\
    <table width="100%" cellpadding="0" cellspacing="0" border='1' align='center'>
    <tr bgcolor="#0099ff">
        <td align='center' height='30' colspan='4'>DAILY ATTENDANCE REPORT DETAILS</td>
    </tr>
    <tr bgcolor="#80ccff">
        <th align='center' height='30'>Employee ID</th>
        <th align='center' height='30'>Name</th>
        <th align='center' height='30'>Attendance Time</th>
        <th align='center' height='30'>Status</th>
        </tr>"""
for row in rows:
    html = html + "<tr align='center'>"
    for col in row:
        html = html + "<td>" + col.replace(" ", "") + "</td>"
    html = html + "</tr>"
html = html + "</table>"

#.format(val1 = '1513605', val2 = dict['1513605'])
print(html)

#htmlcode.format(val= dict['1513605'])
msg = email.message.Message()
msg['Subject'] = 'Daily Attendance Report from ReMark: %d / %d / %d' %(now.day, now.month, now.year)
msg['From'] = 'remark.techiz@gmail.com'
msg['To'] = 'remark.techiz@gmail.com'
password = "remarkqaz@11"
msg.add_header('Content-Type', 'text/html')
msg.set_payload(html)
try:
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.starttls()
    s.login(msg['From'], password)
    receiver = ['sagarjit.dash900@gmail.com', 'deepikab666@gmail.com', 'gmanik1212@gmail.com']
    s.sendmail(msg['From'], receiver , msg.as_string())
    print("Mail has been sent for %d/%d/%d" %(now.day, now.month, now.year)+' successfully')
    s.close()
except:
    print("Unable to send report")
    raise






msg = MIMEMultipart()
msg['From'] = 'sagarjit.dash900@gmail.com'
msg['To'] = 'sagarjit.dash900@gmail.com'
msg['Subject'] = 'Monthly employee report %d/%d ' % (now.month, now.year)

db = cx_Oracle.connect('system/tiger@127.0.0.1/xe')
cursor = db.cursor()
cursor.execute("select EMP_ID, EMP_NAME from REMARK_EMP_REG_INFO where EMP_ID is not null")
data = cursor.fetchall()

report = tempfile.NamedTemporaryFile()
report.write("<table>")
for row in cursor:
  report.write("<tr>")
  for field in row:
    report.write("<td>%s</td>" % field)
  report.write("</tr>")
report.write("</table>")
report.flush()
cursor.close()
db.close()

attachment = MIMEBase('application', 'vnd.ms-excel')
report.file.seek(0)
attachment.set_payload(report.file.read())
encode_base64(attachment)
attachment.add_header('Content-Disposition', 'attachment;filename=emp_report_%d_%d.xls' % (now.month, now.year))
msg.attach(attachment)

s = smtplib.SMTP('smtp.gmail.com', 587)
s.starttls()
s.login("remark.techiz@gmail.com", "remarkqaz@11")
s.sendmail('remark.techiz@gmail.com', ['sagarjit.dash900@gmail.com'] , msg.as_string())
s.quit()
