# IMAGE ANALYZER

This module is created to analyze the similarity as well as dissimilarity of images based on the faces and 
send the attendance status to the database. Attendance is marked according to the status.

The functionalities which are involved in this module:

      1. Retrieval of images with employeeID from database
	  2. Store database images to Temp dir
	  3. Image preparation
	  4. Access images from app local dir to dictionary in script
	  5. Comparision of images by matching with EmpID.
	  6. Send status to DB
      
##  Overall Process Flow Diagram

     FACIAL IMAGES ACCESS
          FROM DB
             |
             |__ STORE IMAGES IN TEMP DIR
                      |
                      |__ ACCESS IMAGES FROM TEMP
                           (MULTI KNOWN IMAGES)
                                  |
                                  |__ ACCESS IMAGES FROM LOCAL
                                    (SINGLE UNKNOWN IMAGE)
                                           |
                                           |__ DETECTION & RECOGNITION
                                                      |
                                                      |__ SEND STATUS TO DB
                                                    
            


## Retrieval of images with employeeID from database(Oracle)

   The images(currently 5 images) of employee which are stored in **ReMark_User_Reg_Details** table and images 
   are taken at the time of user registration from android application. All the images are required to retrieve 
   through python script inorder to start the recognition of the images. To store the images we use oracle database 
   from where images are retrieved. The images which we get, basically are in blob(Binary Large Object) format. 
   SQL query is fired to get the images and corresponding employee id. All these details are temporarily stored in 
   python dictionary.

```  

def db_search_img_to_dict():
    #oracle images from database to python dictionary
    stmt = 'SELECT {1},{2},{3},{4},{5},{6} FROM {0}'.format(config.reg_table,config.emp_id,config.face_image1,
     config.face_image2, config.face_image3,config.face_image4,config.face_image5)
    cur.execute(stmt)
    row = cur.fetchall()
    list_id_clob = row
    print("Lict_id_clob--> ",list_id_clob)
    empID_from_tuple_list = [i[0] for i in row]
    print empID_from_tuple_list
    clob_from_tuple_list = [i[1:6] for i in row]
    print clob_from_tuple_list
    dict_id_clob = dict((z[0],list(z[1:])) for z in zip(empID_from_tuple_list, clob_from_tuple_list))
    print("db_search_oracle_img_to_dict",  dict_id_clob)
    oracle_img_dict_to_tempDir(dict_id_clob)

```

## Store database images to temporary directory

   This step indicates that all the images which are accessed, are stored in temporary directory on the app server path.
   The clob image is first converted to image format and image name is created by the employee id and path name is declared
   by app server path. The images are saved in the path with the corresponding image name.
   The sub steps which are involved in the below code:
   1. Taking input(empid and clob images in dictionary)
   2. Setting directory path where images will be stored in temporary directory.
   3. Clob files are converted in jpg format and write the files into temp dir.
   
```

def oracle_img_dict_to_tempDir(dict_id_clob):
    print ('oracle_img_dict_to_tempDir-->',dict_id_clob)
    for eid in dict_id_clob.keys():
        sub_folder = "C:\\Users\HP\\Desktop\\oracle_temp_dir\\" + "E" + eid
        if os.path.exists(sub_folder):
            shutil.rmtree(sub_folder)
        for clobs in dict_id_clob[eid]:
            print('CLOBS-->',clobs)
            os.mkdir(sub_folder)
            #os.mkdir("training_data\\" + eid)
            for i in range(5):
                #path = "C:\\Users\HP\\Desktop\\oracle_temp_db\\" + eid + "\\"+ eid + "_" + str(i + 1) +".bmp"
                path = sub_folder + "\\" + eid + str(i + 1) + ".jpg"
                #path = "training_data\\" + eid + "\\" + str(i) + ".jpg"
                imageFile = open(path, 'wb')
                imageFile.write(clobs[i].read())
                imageFile.close()
```

## Image preparation

   The next step is to access the images from temporary directory into the script. Here we first read the path of all images 
   from temp directory on app server path and then accessing the image names. Finally all the image name and images(image path)
   are stored in dictionary as key-value pair.
   Steps involved:
   1. 
   
```

def prepare_training_data(temp_folder_path):
    print("data_folder_path-->", temp_folder_path)
    dirs = os.listdir(temp_folder_path)
    print dirs
    faces = []
    labels = []
    for dir_name in dirs:
        if not dir_name.startswith("E"):
            continue
        label = int(dir_name.replace("E", ""))
        emp_dir_path = temp_folder_path + "\\" + dir_name
        print("emp_dir_path-->",emp_dir_path)
        emp_images_names = os.listdir(emp_dir_path)
        print("emp_images_names-->",emp_images_names)
        for image_name in emp_images_names:
            # ignore system files like .DS_Store
            if image_name.startswith("."):
                continue
            image_path = emp_dir_path + "\\" + image_name
            print("Image path-->",image_path)
            image = cv2.imread(image_path)
            cv2.imshow("Training on image...", image)
            cv2.waitKey(300)
            # detect face
            face, rect = detect_face(image)
            #cv2.imshow('img', face)
            faces.append(face)
            labels.append(label)
    print("Faces-",faces)
    print ("Labels-",labels)
    return faces, labels


```

  
   




	  