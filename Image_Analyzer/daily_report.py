import datetime
import smtplib
import email.message

#today = datetime.datetime.now().strftime('%d-%m-%y %H:%M:%S')
now = datetime.datetime.now()

marked_list=[('1513605','Sagarjit \tDas','08/10/2018', '9:45 AM','', '5:25 PM','M'),
      ('1513610','Ramana \tKotlin','08/10/2018','9:39 AM','', '5:21 PM', 'M'),
      ('1513612', 'Deepika \tBansal', '08/10/2018','9:32 AM','', '5:23 PM', 'M'),
      ('1513613', 'Manik \tGupta', '08/10/2018', '9:47 AM', '1:17 PM','', 'M'),
      ('1513615', 'Nand \tKishor', '08/10/2018', '9:55 AM','', '5:27 PM', 'M')]

unmarked_list =[('1513614', 'Mohit \tTewari', '08/10/2018', '9:45 AM','', '5:13 PM', 'UM'),
                ('1513620', 'Gurpreet \tSingh', '08/10/2018', '9:45 AM','', '5:17 PM', 'UM')]

no_Of_Marked = len(marked_list)
no_Of_Unmarked = len(unmarked_list)
total_attempts = len(marked_list) + len(unmarked_list)


#html code
html = """\
    <table width="100%" cellpadding="0" cellspacing="0" border='1' align='center'>
    <tr bgcolor="#0099ff" style='color: black;'>
        <td align='center' height='20' colspan='7'><b>DAILY ATTENDANCE REPORT DETAILS</b></td>
    </tr>

    <tr bgcolor="#4db8ff" style='color: black;'>
        <td align='center' height='20' colspan='7'><b> SUMMARY STATS </b></td>
    </tr>

    <tr bgcolor="#80ccff" style='color: black;'>
        <td align='center' colspan='2'><b>Attempts</b></td>
        <td align='center' colspan='2'><b>Success</b></td>
        <td align='center' colspan='3'><b>Failed</b></td>
    </tr>

    <tr style='color: black;'>
        <td align='center' colspan='2'><b> {x} </b></td>
        <td align='center' colspan='2'><b> {y} </b></td>
        <td align='center' colspan='3'><b> {z} </b></td>
    </tr>

    <tr bgcolor="#4db8ff" style='color: black;'>
        <td align='center' height='20' colspan='7'><b> DETAILED STATS </b></td>
    </tr>

    <tr bgcolor="#80ccff" style='color: black;'>
        <th align='center'><b>Employee ID</b></th>
        <th align='center'><b>Name</b></th>
        <th align='center'><b>Date</b></th>
        <th align='center'><b>In Time</b></th>
        <th align='center'><b>Half Day</b></th>
        <th align='center'><b>Out Time</b></th>
        <th align='center'><b>Status</b></th>
        </tr>
        """.format(x = total_attempts, y = no_Of_Marked, z = no_Of_Unmarked)

#data_uri = open('correct.png', 'rb').read().encode('base64').replace('\n', '')
#img_tag = '<img src="data:image/png;base64,{0}">'.format(data_uri)
#print("Image Tag--", img_tag)

for row in marked_list:
    html = html + "<tr align='center' height='10' >"
    for col in row:
        #res = [item for item in rows if item[3]]
        if col.replace(" ", "") == 'M':
            #html = html + "<td style='color: green;'><b>" + col.replace(" ", "") + "</b></td>"
            html = html + "<td> <img src='https://png.icons8.com/material/50/2ecc71/checkmark.png' width='15' height='15' /></td>"
            #html = html + "<td> <button type='button' height='10'>B</button> </td>"
            #html = html + "<td>" + img_tag + "</td>"
        else:
            html = html + "<td style='color: black;'>" + col.replace(" ", "") + "</td>"
    html = html + "</tr>"


html = html + """\
    <tr bgcolor="#80ccff">
        <td height='3' colspan='7'></td>
        </tr>
        """

for row in unmarked_list:
    html = html + "<tr align='center'>"
    for col in row:
        #res = [item for item in rows if item[3]]
        print("from unmark--",col.replace(" ",""))
        #print(res)
        if col.replace(" ","") == 'UM':
            #html = html + "<td style='color: red;'><b>" + col.replace(" ", "") + "</b></td>"
            html = html + "<td> <img src='https://png.icons8.com/material/50/c0392b/delete-sign.png' width='15' height='15' /></td>"
            #html = html + "<td> <button type='button' height='10'>B</button> </td>"
        else:
            html = html + "<td style='color: black;'>" + col.replace(" ", "") + "</td>"
    html = html + "</tr>"

html = html + "</table>"

#.format(val1 = '1513605', val2 = dict['1513605'])
print(html)




#mail sending code
msg = email.message.Message()
msg['Subject'] = 'Daily Attendance Report from ReMark: %d / %d / %d' %(now.day, now.month, now.year)
msg['From'] = 'remark.techiz@gmail.com'
msg['To'] = 'remark.techiz@gmail.com'
password = "remarkqaz@11"
msg.add_header('Content-Type', 'text/html')
msg.set_payload(html)
try:
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.starttls()
    s.login(msg['From'], password)
    receiver = ['sagarjit.dash900@gmail.com', 'deepikab666@gmail.com']
    s.sendmail(msg['From'], receiver , msg.as_string())
    print("Mail has been sent for %d/%d/%d" %(now.day, now.month, now.year)+' successfully')
    s.close()
except:
    print("Unable to send report")
    raise
