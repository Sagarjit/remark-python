import cx_Oracle
import datetime
import smtplib
import tempfile
from email.encoders import encode_base64
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart



today = datetime.datetime.now()

msg = MIMEMultipart()
msg['From'] = 'sagarjit.dash900@gmail.com'
msg['To'] = 'sagarjit.dash900@gmail.com'
msg['Subject'] = 'Monthly employee report %d/%d ' % (today.month, today.year)
body = "Body_of_the_mail"

db = cx_Oracle.connect('system/tiger@127.0.0.1/xe')
cursor = db.cursor()
cursor.execute("select EMP_ID, EMP_NAME from REMARK_EMP_REG_INFO where EMP_ID is not null")
data = cursor.fetchall()

report = tempfile.NamedTemporaryFile()
report.write("<table>")
for row in cursor:
  report.write("<tr>")
  for field in row:
    report.write("<td>%s</td>" % field)
  report.write("</tr>")
report.write("</table>")
report.flush()
cursor.close()
db.close()


msg.attach(MIMEText(body, 'plain'))
filename = 'output.csv'
#attachment = open('C:\\Users\\HP\\Desktop\\output.csv', 'rb')
p = MIMEBase('application', 'octet-stream')
report.file.seek(0)
p.set_payload(report.file.read())
encode_base64(p)
p.add_header('Content-Disposition', 'attachment;filename=emp_report_%d_%d.xls' % (today.month, today.year))
msg.attach(p)

'''
part = MIMEBase('application', "octet-stream")
part.set_payload(open("WorkBook3.xlsx", "rb").read())
encode_base64(part)
part.add_header('Content-Disposition', 'attachment; filename="WorkBook3.xlsx"')
msg.attach(part)
'''

try:
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.starttls()
    s.login("remark.techiz@gmail.com", "remarkqaz@11")
    s.sendmail('remark.techiz@gmail.com', ['sagarjit.dash900@gmail.com'] , msg.as_string())
    s.quit()
except:
    print("Unable to send report")
    raise
