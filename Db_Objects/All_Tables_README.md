# TABLES DESCRIPTIONS FOR REMARK

## Table Signatures

    1. ReMark_User_Reg_Details
    2. ReMark_User_Cred_Verification
    3. ReMark_Users_Daily_Report
    4. ReMark_Users_Monthly_Report
    5. ReMark_User_Log_Details
    
    

---------------------------------------------------------------------------------------------------------------------------

    
##  ReMark_User_Reg_Details

__**Attributes**__
    
    1. EMP_ID - Employee ID which is unique primary key.
    2. EMP_EMAIL - Official email id of each employee.
    3. EMP_MOB - phone number of employee
    4. EMP_NAME - Name of the Employee.
    5. EMP_DOB - Date of Birth of the Employee.
    6. EMP_CLGNAME - Employee college name where he/she is in service (BITS Pilani).
    7. EMP_DEPT - Department name of the employee (CSE/ IT etc.).
    8. EMP_DESG - Designation of employee (Assistant Professor/ Associate Professor/ Professor/ Cleark/ Manager/ Admin etc).
    9. EMP_FACE_IMGS - Five facial images of each employee.
    10. EMP_IMEI - Unique device id for single time registration for each employee.
    11. CREATED_TS - Time of the registration.
    12. PASSWORD - unique password for the account
    13. CONFIRM_PASSWORD - same password as above

## ReMark_User_Cred_Verification

__**Attributes**__
   
    1. EMP_ID - Employee ID which is unique primary key which is already exists in table.
    2. EMP_EMAIL - Official email id of each employee which is already exists in table.
    3. EMP_MOB - Mobile number of employee which is already exists in table.
    4. OTP - randomly generated 4 digits number.
    5. FLAG - Indicates that the verification has been completed/not(Value- T/ F, Default- F)
    
**Note :** User will enter employee id in verfication layout and will wait for OTP which will be sent to 
           the registered mobile number. After successful of OPT verification, flag value will be changed 
           to 'T' and all required info(EMP_ID, EMP_EMAIL, EMP_MOB) will be fetched from db, registration 
           layout will be opened and values will be set in corresponding fields.
    

## ReMark_Users_Daily_Report

__**Attributes**__

    1. EMP_ID - Employee ID which is unique primary key which is already exists in table.
    2. EMP_NAME - Employee ID which is unique primary key which is already exists in table.
    3. DATE - Date of the attendance.
    4. IN_TIME - Check in time of the employee. 
    5. HALF_DAY - half day check out time of the employee.  
    6. OUT_TIME - Check out time of the employee.
    7. STATUS - Two states: either 'M' or 'NM'. M- Marked, NM- Not Marked. 
    
**Note :** Cron Job(daily_report.py) will be scheduled to generate the daily report and Html mail will 
           be sent to Admin.

## ReMark_Users_Monthly_Report

__**Attributes**__

    1. EMP_ID - Employee ID which is unique primary key.
    2. EMP_NAME - Employee ID which is unique primary key.
    3. STATUS - 'M' or 'NM' for 1 month

**Note :** Cron Job(mothly_report.py) will be scheduled to generate the monthly report and attachment(excel file) 
           mail will be sent to Admin.
   
   
## ReMark_User_Log_Details

__**Attributes**__

     1. ACCOUNT_ID - Employee ID which is unique primary key.
     2. APP_LAST_LOGIN_DTIME - time when user logged in the app for last time, i.e last timestamp.
     2. APP_LOGIN_DTIME - Current application login  date & time for user.
     3. APP_ATTENDANCE_START_TIME - Time when user send the unknown facial image from application.
     4. PY_ATTENDANCE_START_TIME - Time when the script will start it's recognition.
     5. PY_ATTENDANCE_END_TIME - Time when the script will end it's recognition.
     6. PY_TTL_PROCESSING_TIME - Total time required for processing any particular unknown image.
     

**Note :** The log info. which will be captured from ReMark App:
               ACCOUNT_ID, APP_LAST_LOGIN_DTIME, APP_LOGIN_DTIME, APP_ATTENDANCE_START_TIME
           The log info. which will be captured from python script:
               PY_ATTENDANCE_START_TIME, PY_ATTENDANCE_END_TIME, PY_TTL_PROCESSING_TIME

     









    